-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-05-2017 a las 23:51:03
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `co`
--
CREATE DATABASE IF NOT EXISTS `co` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `co`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admon`
--

CREATE TABLE IF NOT EXISTS `admon` (
  `id` int(11) NOT NULL,
  `usuario` varchar(15) NOT NULL,
  `contra` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `admon`:
--

--
-- Volcado de datos para la tabla `admon`
--

INSERT INTO `admon` (`id`, `usuario`, `contra`) VALUES
(1, 'Admon', '_ª¬âÞâ¼·œäö¶èJ'),
(2, 'Usuario', '€´†ié–ÛÝ´%| ×‡Ù');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencias`
--

CREATE TABLE IF NOT EXISTS `asistencias` (
  `id` int(11) NOT NULL,
  `asistencia` varchar(4) NOT NULL,
  `fecha` date NOT NULL,
  `id_empleado` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `asistencias`:
--   `id_empleado`
--       `empleados` -> `id`
--

--
-- Volcado de datos para la tabla `asistencias`
--

INSERT INTO `asistencias` (`id`, `asistencia`, `fecha`, `id_empleado`) VALUES
(1, 'D', '2017-04-20', 9),
(2, 'J', '2017-04-20', 10),
(3, 'D', '2017-04-20', 11),
(4, 'A', '2017-04-20', 12),
(5, 'D', '2017-04-20', 13),
(6, 'D', '2017-04-21', 9),
(7, 'J', '2017-04-21', 10),
(8, 'D', '2017-04-21', 11),
(9, 'T', '2017-04-21', 12),
(10, 'D', '2017-04-21', 13),
(11, 'D', '2017-04-22', 9),
(12, 'T-Ab', '2017-04-22', 10),
(13, 'D', '2017-04-22', 11),
(14, 'D', '2017-04-22', 12),
(15, 'F', '2017-04-22', 13),
(16, 'D', '2017-04-23', 9),
(17, 'D', '2017-04-23', 10),
(18, 'A', '2017-04-23', 11),
(19, 'D', '2017-04-23', 12),
(20, 'Ab', '2017-04-23', 13),
(21, 'T', '2017-04-24', 9),
(22, 'D', '2017-04-24', 10),
(23, 'T-Ab', '2017-04-24', 11),
(24, 'D', '2017-04-24', 12),
(25, 'F', '2017-04-24', 13),
(26, 'A', '2017-04-25', 9),
(27, 'D', '2017-04-25', 10),
(28, 'Ab', '2017-04-25', 11),
(29, 'D', '2017-04-25', 12),
(30, 'D', '2017-04-25', 13),
(31, 'F', '2017-05-02', 9),
(32, 'D', '2017-05-02', 10),
(33, 'F', '2017-05-02', 11),
(34, 'D', '2017-05-02', 12),
(35, 'D', '2017-05-02', 13),
(36, 'F', '2017-05-03', 9),
(37, 'D', '2017-05-03', 10),
(38, 'D', '2017-05-03', 11),
(39, 'F', '2017-05-03', 12),
(40, 'J', '2017-05-03', 13),
(41, 'J', '2017-05-04', 9),
(42, 'F', '2017-05-04', 10),
(43, 'D', '2017-05-04', 11),
(44, 'J', '2017-05-04', 12),
(45, 'J', '2017-05-04', 13),
(46, 'D', '2017-05-05', 9),
(47, 'F', '2017-05-05', 10),
(48, 'D', '2017-05-05', 11),
(49, 'F', '2017-05-05', 12),
(50, 'D', '2017-05-05', 13),
(51, 'F', '2017-05-09', 9),
(52, 'D', '2017-05-09', 10),
(53, 'F', '2017-05-09', 11),
(54, 'D', '2017-05-09', 12),
(55, 'D', '2017-05-09', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `id` int(11) NOT NULL,
  `nombre` varchar(25) NOT NULL,
  `ap` varchar(25) NOT NULL,
  `am` varchar(25) NOT NULL,
  `direccion` varchar(60) NOT NULL,
  `img_h` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `empleados`:
--

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `nombre`, `ap`, `am`, `direccion`, `img_h`) VALUES
(9, 'Daniel0', 'H', 'J', 'D', 'h_9.fpt'),
(10, 'Daniel1', 'H', 'J', 'D', '001'),
(11, 'Daniel2', 'H', 'J', 'D', '002'),
(12, 'Daniel3', 'H', 'J', 'D', '003'),
(13, 'Daniel4', 'H', 'J', 'D', '004');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE IF NOT EXISTS `horarios` (
  `id_empleado` int(11) NOT NULL,
  `dia` varchar(9) NOT NULL,
  `he` time DEFAULT NULL,
  `hs` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `horarios`:
--   `id_empleado`
--       `empleados` -> `id`
--

--
-- Volcado de datos para la tabla `horarios`
--

INSERT INTO `horarios` (`id_empleado`, `dia`, `he`, `hs`) VALUES
(9, 'Lunes', '08:00:00', '20:00:00'),
(9, 'Martes', '08:00:00', '20:00:00'),
(9, 'Miercoles', '08:00:00', '20:00:00'),
(10, 'Jueves', '08:00:00', '20:00:00'),
(10, 'Viernes', '08:00:00', '20:00:00'),
(10, 'Sabado', '08:00:00', '20:00:00'),
(11, 'Domingo', '08:00:00', '20:00:00'),
(11, 'Lunes', '08:00:00', '20:00:00'),
(11, 'Martes', '08:00:00', '20:00:00'),
(12, 'Miercoles', '08:00:00', '20:00:00'),
(12, 'Jueves', '08:00:00', '20:00:00'),
(12, 'Viernes', '08:00:00', '20:00:00'),
(13, 'Sabado', '08:00:00', '20:00:00'),
(13, 'Domingo', '08:00:00', '20:00:00'),
(13, 'Lunes', '08:00:00', '20:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `justifics`
--

CREATE TABLE IF NOT EXISTS `justifics` (
  `id` int(11) NOT NULL,
  `id_asistencia` int(11) NOT NULL,
  `tipo` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `justifics`:
--   `id_asistencia`
--       `asistencias` -> `id`
--

--
-- Volcado de datos para la tabla `justifics`
--

INSERT INTO `justifics` (`id`, `id_asistencia`, `tipo`) VALUES
(3, 40, 'Médico'),
(4, 45, 'Otro'),
(5, 44, 'Personal'),
(6, 41, 'capa');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `id` int(11) NOT NULL,
  `tipo` varchar(7) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `id_empleado` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `registros`:
--   `id_empleado`
--       `empleados` -> `id`
--

--
-- Volcado de datos para la tabla `registros`
--

INSERT INTO `registros` (`id`, `tipo`, `fecha`, `hora`, `id_empleado`) VALUES
(11, 'Entrada', '2017-04-20', '07:00:00', 12),
(12, 'Salida', '2017-04-20', '21:00:00', 12),
(13, 'Entrada', '2017-04-21', '08:00:00', 10),
(14, 'Entrada', '2017-04-21', '09:00:00', 12),
(15, 'Salida', '2017-04-21', '19:00:00', 10),
(16, 'Salida', '2017-04-21', '20:00:00', 12),
(17, 'Entrada', '2017-04-22', '09:00:00', 10),
(18, 'Salida', '2017-04-22', '19:00:00', 10),
(19, 'Entrada', '2017-04-23', '07:00:00', 11),
(20, 'Entrada', '2017-04-23', '08:00:00', 13),
(21, 'Salida', '2017-04-23', '19:00:00', 13),
(22, 'Salida', '2017-04-23', '21:00:00', 11),
(23, 'Entrada', '2017-04-24', '09:00:00', 9),
(24, 'Entrada', '2017-04-24', '09:30:00', 11),
(25, 'Salida', '2017-04-24', '19:00:00', 11),
(26, 'Salida', '2017-04-24', '20:00:00', 9),
(27, 'Entrada', '2017-04-25', '07:00:00', 9),
(28, 'Entrada', '2017-04-25', '08:00:00', 11),
(29, 'Salida', '2017-04-25', '19:00:00', 11),
(30, 'Salida', '2017-04-25', '21:00:00', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `salarios`
--

CREATE TABLE IF NOT EXISTS `salarios` (
  `id_empleado` int(11) NOT NULL,
  `tipo` varchar(4) NOT NULL,
  `salario` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELACIONES PARA LA TABLA `salarios`:
--   `id_empleado`
--       `empleados` -> `id`
--

--
-- Volcado de datos para la tabla `salarios`
--

INSERT INTO `salarios` (`id_empleado`, `tipo`, `salario`) VALUES
(9, 'A', 1),
(9, 'T', 1),
(9, 'Ab', 1),
(9, 'T-Ab', 1),
(9, 'J', 1),
(9, 'D', 0),
(9, 'F', 0),
(10, 'A', 2),
(10, 'T', 2),
(10, 'Ab', 2),
(10, 'T-Ab', 2),
(10, 'J', 1),
(10, 'D', 0),
(10, 'F', 0),
(11, 'A', 1),
(11, 'T', 1),
(11, 'Ab', 1),
(11, 'T-Ab', 1),
(11, 'J', 1),
(11, 'D', 0),
(11, 'F', 0),
(12, 'A', 1),
(12, 'T', 1),
(12, 'Ab', 1),
(12, 'T-Ab', 1),
(12, 'J', 1),
(12, 'D', 0),
(12, 'F', 0),
(13, 'A', 1),
(13, 'T', 1),
(13, 'Ab', 1),
(13, 'T-Ab', 1),
(13, 'J', 2),
(13, 'D', 0),
(13, 'F', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admon`
--
ALTER TABLE `admon`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleados`
--
ALTER TABLE `empleados`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `justifics`
--
ALTER TABLE `justifics`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `registros`
--
ALTER TABLE `registros`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admon`
--
ALTER TABLE `admon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `asistencias`
--
ALTER TABLE `asistencias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT de la tabla `empleados`
--
ALTER TABLE `empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `justifics`
--
ALTER TABLE `justifics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `registros`
--
ALTER TABLE `registros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
