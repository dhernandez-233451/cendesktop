﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CentralOfertas
{
    public partial class CuentaAdmin : Form
    {
        public CuentaAdmin()
        {
            InitializeComponent();
            cdatos();
        }

        void cdatos()
        {
            dgv.DataSource = ctrl.recibir("SELECT usuario, CAST(AES_DECRYPT(contra,'cod0417') AS CHAR(15))contra FROM admon WHERE id=1;");
            txtNU.Text = dgv.Rows[0].Cells[0].Value.ToString();
            txtNC.Text = dgv.Rows[0].Cells[1].Value.ToString();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!txtNU.Text.Equals("") && !txtNC.Text.Equals("")) if (txtNC.Text.Equals(txtCC.Text)) if (ctrl.enviar("UPDATE admon SET usuario='" + txtNU.Text + "', contra=AES_ENCRYPT('" + txtNC.Text + "','cod0417') WHERE id=1;") > 0) MessageBox.Show("Actualización Exitosa","Información",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    else MessageBox.Show("Error al Actualizar","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                else MessageBox.Show("Las contraseñas no coinciden","Advertencia",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
        }

        private void btnNU_Click(object sender, EventArgs e)
        {
            new CuentaUsuario().Show();
        }
    }
}
