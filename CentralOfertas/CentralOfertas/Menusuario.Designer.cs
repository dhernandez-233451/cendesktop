﻿namespace CentralOfertas
{
    partial class Menusuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdmon = new System.Windows.Forms.PictureBox();
            this.btnEst = new System.Windows.Forms.PictureBox();
            this.btnSalir = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdmon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdmon
            // 
            this.btnAdmon.BackColor = System.Drawing.Color.Transparent;
            this.btnAdmon.Location = new System.Drawing.Point(141, 145);
            this.btnAdmon.Name = "btnAdmon";
            this.btnAdmon.Size = new System.Drawing.Size(134, 50);
            this.btnAdmon.TabIndex = 0;
            this.btnAdmon.TabStop = false;
            this.btnAdmon.Click += new System.EventHandler(this.btnAdmon_Click);
            // 
            // btnEst
            // 
            this.btnEst.BackColor = System.Drawing.Color.Transparent;
            this.btnEst.Location = new System.Drawing.Point(141, 213);
            this.btnEst.Name = "btnEst";
            this.btnEst.Size = new System.Drawing.Size(134, 50);
            this.btnEst.TabIndex = 1;
            this.btnEst.TabStop = false;
            this.btnEst.Click += new System.EventHandler(this.btnEst_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.Transparent;
            this.btnSalir.Location = new System.Drawing.Point(160, 275);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(97, 47);
            this.btnSalir.TabIndex = 2;
            this.btnSalir.TabStop = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // Menusuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CentralOfertas.Properties.Resources.menuusuario;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(417, 351);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnEst);
            this.Controls.Add(this.btnAdmon);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Menusuario";
            this.Text = "Menusuario";
            this.TransparencyKey = System.Drawing.SystemColors.Control;
            ((System.ComponentModel.ISupportInitialize)(this.btnAdmon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox btnAdmon;
        private System.Windows.Forms.PictureBox btnEst;
        private System.Windows.Forms.PictureBox btnSalir;
    }
}