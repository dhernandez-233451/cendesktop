﻿namespace CentralOfertas
{
    partial class MenuConfi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdmon = new System.Windows.Forms.PictureBox();
            this.btnCuenta = new System.Windows.Forms.PictureBox();
            this.btnEst = new System.Windows.Forms.PictureBox();
            this.btnSalir = new System.Windows.Forms.PictureBox();
            this.btnJus = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdmon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCuenta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnJus)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAdmon
            // 
            this.btnAdmon.BackColor = System.Drawing.Color.Transparent;
            this.btnAdmon.Location = new System.Drawing.Point(123, 119);
            this.btnAdmon.Name = "btnAdmon";
            this.btnAdmon.Size = new System.Drawing.Size(133, 40);
            this.btnAdmon.TabIndex = 0;
            this.btnAdmon.TabStop = false;
            this.btnAdmon.Click += new System.EventHandler(this.btnAdmon_Click);
            // 
            // btnCuenta
            // 
            this.btnCuenta.BackColor = System.Drawing.Color.Transparent;
            this.btnCuenta.Location = new System.Drawing.Point(123, 181);
            this.btnCuenta.Name = "btnCuenta";
            this.btnCuenta.Size = new System.Drawing.Size(133, 45);
            this.btnCuenta.TabIndex = 1;
            this.btnCuenta.TabStop = false;
            this.btnCuenta.Click += new System.EventHandler(this.btnCuenta_Click);
            // 
            // btnEst
            // 
            this.btnEst.BackColor = System.Drawing.Color.Transparent;
            this.btnEst.Location = new System.Drawing.Point(123, 247);
            this.btnEst.Name = "btnEst";
            this.btnEst.Size = new System.Drawing.Size(133, 38);
            this.btnEst.TabIndex = 2;
            this.btnEst.TabStop = false;
            this.btnEst.Click += new System.EventHandler(this.btnEst_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.Transparent;
            this.btnSalir.Location = new System.Drawing.Point(148, 390);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(84, 35);
            this.btnSalir.TabIndex = 3;
            this.btnSalir.TabStop = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnJus
            // 
            this.btnJus.BackColor = System.Drawing.Color.Transparent;
            this.btnJus.Location = new System.Drawing.Point(123, 302);
            this.btnJus.Name = "btnJus";
            this.btnJus.Size = new System.Drawing.Size(135, 38);
            this.btnJus.TabIndex = 4;
            this.btnJus.TabStop = false;
            this.btnJus.Click += new System.EventHandler(this.btnJus_Click);
            // 
            // MenuConfi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CentralOfertas.Properties.Resources.menuConfi2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(382, 481);
            this.Controls.Add(this.btnJus);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnEst);
            this.Controls.Add(this.btnCuenta);
            this.Controls.Add(this.btnAdmon);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MenuConfi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuConfi";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((System.ComponentModel.ISupportInitialize)(this.btnAdmon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCuenta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnJus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox btnAdmon;
        private System.Windows.Forms.PictureBox btnCuenta;
        private System.Windows.Forms.PictureBox btnEst;
        private System.Windows.Forms.PictureBox btnSalir;
        private System.Windows.Forms.PictureBox btnJus;
    }
}