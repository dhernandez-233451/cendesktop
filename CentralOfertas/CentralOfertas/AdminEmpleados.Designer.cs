﻿namespace CentralOfertas
{
    partial class AdminEmpleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvE = new System.Windows.Forms.DataGridView();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel = new System.Windows.Forms.Panel();
            this.btnAceptarH = new System.Windows.Forms.Button();
            this.btnElH = new System.Windows.Forms.Button();
            this.btnAgH = new System.Windows.Forms.Button();
            this.dgvH = new System.Windows.Forms.DataGridView();
            this.dgvH_Cdia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DgvH_CHE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DgvH_CHS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtHS = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtHE = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDia = new System.Windows.Forms.ComboBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtAP = new System.Windows.Forms.TextBox();
            this.txtAM = new System.Windows.Forms.TextBox();
            this.txtDirec = new System.Windows.Forms.TextBox();
            this.dgvx = new System.Windows.Forms.DataGridView();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnHorario = new System.Windows.Forms.PictureBox();
            this.btnAH = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btn_sal = new System.Windows.Forms.Button();
            this.huella = new System.Windows.Forms.PictureBox();
            this.lblInstrucciones = new System.Windows.Forms.Label();
            this.btnSalario = new System.Windows.Forms.PictureBox();
            this.panelSalario = new System.Windows.Forms.Panel();
            this.btnSAceptar = new System.Windows.Forms.Button();
            this.txtSTAb = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtSAb = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtST = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSA = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSF = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSD = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSJ = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvE)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvx)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHorario)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.huella)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalario)).BeginInit();
            this.panelSalario.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvE
            // 
            this.dgvE.AllowUserToAddRows = false;
            this.dgvE.AllowUserToDeleteRows = false;
            this.dgvE.AllowUserToOrderColumns = true;
            this.dgvE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvE.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvE.Location = new System.Drawing.Point(6, 22);
            this.dgvE.Name = "dgvE";
            this.dgvE.ReadOnly = true;
            this.dgvE.Size = new System.Drawing.Size(551, 278);
            this.dgvE.TabIndex = 0;
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(29, 369);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(93, 29);
            this.btnGuardar.TabIndex = 1;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(160, 369);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(93, 29);
            this.btnSalir.TabIndex = 2;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditar.Location = new System.Drawing.Point(293, 388);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(93, 31);
            this.btnEditar.TabIndex = 3;
            this.btnEditar.Text = "Editar";
            this.btnEditar.UseVisualStyleBackColor = true;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dgvE);
            this.groupBox1.Location = new System.Drawing.Point(293, 82);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(566, 309);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Empleados";
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel.Controls.Add(this.btnAceptarH);
            this.panel.Controls.Add(this.btnElH);
            this.panel.Controls.Add(this.btnAgH);
            this.panel.Controls.Add(this.dgvH);
            this.panel.Controls.Add(this.dtHS);
            this.panel.Controls.Add(this.label4);
            this.panel.Controls.Add(this.label3);
            this.panel.Controls.Add(this.label2);
            this.panel.Controls.Add(this.dtHE);
            this.panel.Controls.Add(this.label1);
            this.panel.Controls.Add(this.cbDia);
            this.panel.Location = new System.Drawing.Point(219, 82);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(530, 271);
            this.panel.TabIndex = 17;
            this.panel.Visible = false;
            // 
            // btnAceptarH
            // 
            this.btnAceptarH.Location = new System.Drawing.Point(220, 236);
            this.btnAceptarH.Name = "btnAceptarH";
            this.btnAceptarH.Size = new System.Drawing.Size(75, 23);
            this.btnAceptarH.TabIndex = 27;
            this.btnAceptarH.Text = "Aceptar";
            this.btnAceptarH.UseVisualStyleBackColor = true;
            this.btnAceptarH.Click += new System.EventHandler(this.btnAceptarH_Click);
            // 
            // btnElH
            // 
            this.btnElH.Location = new System.Drawing.Point(279, 78);
            this.btnElH.Name = "btnElH";
            this.btnElH.Size = new System.Drawing.Size(75, 23);
            this.btnElH.TabIndex = 26;
            this.btnElH.Text = "Eliminar";
            this.btnElH.UseVisualStyleBackColor = true;
            this.btnElH.Click += new System.EventHandler(this.btnElH_Click);
            // 
            // btnAgH
            // 
            this.btnAgH.Location = new System.Drawing.Point(185, 78);
            this.btnAgH.Name = "btnAgH";
            this.btnAgH.Size = new System.Drawing.Size(75, 23);
            this.btnAgH.TabIndex = 25;
            this.btnAgH.Text = "Agregar";
            this.btnAgH.UseVisualStyleBackColor = true;
            this.btnAgH.Click += new System.EventHandler(this.btnAgH_Click);
            // 
            // dgvH
            // 
            this.dgvH.AllowUserToAddRows = false;
            this.dgvH.AllowUserToDeleteRows = false;
            this.dgvH.AllowUserToOrderColumns = true;
            this.dgvH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvH_Cdia,
            this.DgvH_CHE,
            this.DgvH_CHS});
            this.dgvH.Location = new System.Drawing.Point(27, 118);
            this.dgvH.Name = "dgvH";
            this.dgvH.ReadOnly = true;
            this.dgvH.Size = new System.Drawing.Size(474, 112);
            this.dgvH.TabIndex = 24;
            // 
            // dgvH_Cdia
            // 
            this.dgvH_Cdia.HeaderText = "Día";
            this.dgvH_Cdia.Name = "dgvH_Cdia";
            this.dgvH_Cdia.ReadOnly = true;
            // 
            // DgvH_CHE
            // 
            this.DgvH_CHE.HeaderText = "Entrada";
            this.DgvH_CHE.Name = "DgvH_CHE";
            this.DgvH_CHE.ReadOnly = true;
            // 
            // DgvH_CHS
            // 
            this.DgvH_CHS.HeaderText = "Salida";
            this.DgvH_CHS.Name = "DgvH_CHS";
            this.DgvH_CHS.ReadOnly = true;
            // 
            // dtHS
            // 
            this.dtHS.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtHS.Location = new System.Drawing.Point(427, 52);
            this.dtHS.Name = "dtHS";
            this.dtHS.Size = new System.Drawing.Size(88, 20);
            this.dtHS.TabIndex = 23;
            this.dtHS.Value = new System.DateTime(2017, 4, 29, 22, 0, 0, 0);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Constantia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(378, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "H Salida:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Constantia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(207, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "H Entrada:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Constantia", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(172, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "De";
            // 
            // dtHE
            // 
            this.dtHE.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtHE.Location = new System.Drawing.Point(266, 52);
            this.dtHE.Name = "dtHE";
            this.dtHE.Size = new System.Drawing.Size(88, 20);
            this.dtHE.TabIndex = 19;
            this.dtHE.Value = new System.DateTime(2017, 4, 29, 8, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Constantia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(195, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 19);
            this.label1.TabIndex = 1;
            this.label1.Text = "Establecer Horario";
            // 
            // cbDia
            // 
            this.cbDia.FormattingEnabled = true;
            this.cbDia.Items.AddRange(new object[] {
            "Lunes",
            "Martes",
            "Miercoles",
            "Jueves",
            "Viernes",
            "Sabado",
            "Domingo"});
            this.cbDia.Location = new System.Drawing.Point(27, 51);
            this.cbDia.Name = "cbDia";
            this.cbDia.Size = new System.Drawing.Size(121, 21);
            this.cbDia.TabIndex = 0;
            // 
            // txtNom
            // 
            this.txtNom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(114, 100);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(158, 19);
            this.txtNom.TabIndex = 5;
            // 
            // txtAP
            // 
            this.txtAP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAP.Location = new System.Drawing.Point(114, 129);
            this.txtAP.Name = "txtAP";
            this.txtAP.Size = new System.Drawing.Size(158, 19);
            this.txtAP.TabIndex = 6;
            // 
            // txtAM
            // 
            this.txtAM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAM.Location = new System.Drawing.Point(114, 159);
            this.txtAM.Name = "txtAM";
            this.txtAM.Size = new System.Drawing.Size(158, 19);
            this.txtAM.TabIndex = 7;
            // 
            // txtDirec
            // 
            this.txtDirec.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDirec.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDirec.Location = new System.Drawing.Point(114, 188);
            this.txtDirec.Name = "txtDirec";
            this.txtDirec.Size = new System.Drawing.Size(158, 19);
            this.txtDirec.TabIndex = 8;
            // 
            // dgvx
            // 
            this.dgvx.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvx.Location = new System.Drawing.Point(814, 26);
            this.dgvx.Name = "dgvx";
            this.dgvx.Size = new System.Drawing.Size(36, 37);
            this.dgvx.TabIndex = 15;
            this.dgvx.Visible = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Location = new System.Drawing.Point(383, 388);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(93, 31);
            this.btnEliminar.TabIndex = 16;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnHorario
            // 
            this.btnHorario.BackColor = System.Drawing.Color.Transparent;
            this.btnHorario.Location = new System.Drawing.Point(114, 226);
            this.btnHorario.Name = "btnHorario";
            this.btnHorario.Size = new System.Drawing.Size(158, 30);
            this.btnHorario.TabIndex = 18;
            this.btnHorario.TabStop = false;
            this.btnHorario.Click += new System.EventHandler(this.btnHorario_Click);
            // 
            // btnAH
            // 
            this.btnAH.Location = new System.Drawing.Point(114, 312);
            this.btnAH.Name = "btnAH";
            this.btnAH.Size = new System.Drawing.Size(158, 23);
            this.btnAH.TabIndex = 19;
            this.btnAH.Text = "Agregar Huella";
            this.btnAH.UseVisualStyleBackColor = true;
            this.btnAH.Click += new System.EventHandler(this.btnAH_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnCancelar);
            this.panel1.Controls.Add(this.btn_sal);
            this.panel1.Controls.Add(this.huella);
            this.panel1.Controls.Add(this.lblInstrucciones);
            this.panel1.Location = new System.Drawing.Point(185, 125);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(201, 257);
            this.panel1.TabIndex = 20;
            this.panel1.Visible = false;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(108, 230);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btn_sal
            // 
            this.btn_sal.Location = new System.Drawing.Point(12, 230);
            this.btn_sal.Name = "btn_sal";
            this.btn_sal.Size = new System.Drawing.Size(75, 23);
            this.btn_sal.TabIndex = 2;
            this.btn_sal.Text = "Aceptar";
            this.btn_sal.UseVisualStyleBackColor = true;
            this.btn_sal.Click += new System.EventHandler(this.btn_sal_Click);
            // 
            // huella
            // 
            this.huella.BackColor = System.Drawing.Color.White;
            this.huella.Location = new System.Drawing.Point(3, 35);
            this.huella.Name = "huella";
            this.huella.Size = new System.Drawing.Size(195, 189);
            this.huella.TabIndex = 1;
            this.huella.TabStop = false;
            // 
            // lblInstrucciones
            // 
            this.lblInstrucciones.AutoSize = true;
            this.lblInstrucciones.Location = new System.Drawing.Point(6, 9);
            this.lblInstrucciones.Name = "lblInstrucciones";
            this.lblInstrucciones.Size = new System.Drawing.Size(35, 13);
            this.lblInstrucciones.TabIndex = 0;
            this.lblInstrucciones.Text = "label5";
            this.lblInstrucciones.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSalario
            // 
            this.btnSalario.BackColor = System.Drawing.Color.Transparent;
            this.btnSalario.Location = new System.Drawing.Point(113, 267);
            this.btnSalario.Name = "btnSalario";
            this.btnSalario.Size = new System.Drawing.Size(158, 30);
            this.btnSalario.TabIndex = 21;
            this.btnSalario.TabStop = false;
            this.btnSalario.Click += new System.EventHandler(this.btnSalario_Click);
            // 
            // panelSalario
            // 
            this.panelSalario.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelSalario.Controls.Add(this.txtSF);
            this.panelSalario.Controls.Add(this.label12);
            this.panelSalario.Controls.Add(this.txtSD);
            this.panelSalario.Controls.Add(this.label13);
            this.panelSalario.Controls.Add(this.txtSJ);
            this.panelSalario.Controls.Add(this.label14);
            this.panelSalario.Controls.Add(this.btnSAceptar);
            this.panelSalario.Controls.Add(this.txtSTAb);
            this.panelSalario.Controls.Add(this.label10);
            this.panelSalario.Controls.Add(this.txtSAb);
            this.panelSalario.Controls.Add(this.label9);
            this.panelSalario.Controls.Add(this.txtST);
            this.panelSalario.Controls.Add(this.label8);
            this.panelSalario.Controls.Add(this.txtSA);
            this.panelSalario.Controls.Add(this.label7);
            this.panelSalario.Controls.Add(this.label6);
            this.panelSalario.Controls.Add(this.label5);
            this.panelSalario.Location = new System.Drawing.Point(219, 12);
            this.panelSalario.Name = "panelSalario";
            this.panelSalario.Size = new System.Drawing.Size(411, 316);
            this.panelSalario.TabIndex = 28;
            this.panelSalario.Visible = false;
            // 
            // btnSAceptar
            // 
            this.btnSAceptar.Location = new System.Drawing.Point(74, 276);
            this.btnSAceptar.Name = "btnSAceptar";
            this.btnSAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnSAceptar.TabIndex = 10;
            this.btnSAceptar.Text = "Aceptar";
            this.btnSAceptar.UseVisualStyleBackColor = true;
            this.btnSAceptar.Click += new System.EventHandler(this.btnSAceptar_Click);
            // 
            // txtSTAb
            // 
            this.txtSTAb.Location = new System.Drawing.Point(28, 243);
            this.txtSTAb.Name = "txtSTAb";
            this.txtSTAb.Size = new System.Drawing.Size(174, 20);
            this.txtSTAb.TabIndex = 9;
            this.txtSTAb.Text = "1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 227);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Llegar tarde e irse temprano (T-Ab)";
            // 
            // txtSAb
            // 
            this.txtSAb.Location = new System.Drawing.Point(28, 199);
            this.txtSAb.Name = "txtSAb";
            this.txtSAb.Size = new System.Drawing.Size(174, 20);
            this.txtSAb.TabIndex = 7;
            this.txtSAb.Text = "1";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 183);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Irse temprano (Ab)";
            // 
            // txtST
            // 
            this.txtST.Location = new System.Drawing.Point(28, 156);
            this.txtST.Name = "txtST";
            this.txtST.Size = new System.Drawing.Size(174, 20);
            this.txtST.TabIndex = 5;
            this.txtST.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Llegar tarde (T)";
            // 
            // txtSA
            // 
            this.txtSA.Location = new System.Drawing.Point(28, 110);
            this.txtSA.Name = "txtSA";
            this.txtSA.Size = new System.Drawing.Size(174, 20);
            this.txtSA.TabIndex = 3;
            this.txtSA.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Normal (A)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Salario por asistencia:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(37, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(130, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Salario por día";
            // 
            // txtSF
            // 
            this.txtSF.Location = new System.Drawing.Point(221, 199);
            this.txtSF.Name = "txtSF";
            this.txtSF.Size = new System.Drawing.Size(174, 20);
            this.txtSF.TabIndex = 16;
            this.txtSF.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(218, 183);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(45, 13);
            this.label12.TabIndex = 15;
            this.label12.Text = "Falta (F)";
            // 
            // txtSD
            // 
            this.txtSD.Location = new System.Drawing.Point(221, 156);
            this.txtSD.Name = "txtSD";
            this.txtSD.Size = new System.Drawing.Size(174, 20);
            this.txtSD.TabIndex = 14;
            this.txtSD.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(218, 140);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "Descanso (D)";
            // 
            // txtSJ
            // 
            this.txtSJ.Location = new System.Drawing.Point(221, 110);
            this.txtSJ.Name = "txtSJ";
            this.txtSJ.Size = new System.Drawing.Size(174, 20);
            this.txtSJ.TabIndex = 12;
            this.txtSJ.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(218, 94);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(79, 13);
            this.label14.TabIndex = 11;
            this.label14.Text = "Justificación (J)";
            // 
            // AdminEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CentralOfertas.Properties.Resources.Inserta4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(871, 422);
            this.Controls.Add(this.panelSalario);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnHorario);
            this.Controls.Add(this.dgvx);
            this.Controls.Add(this.txtDirec);
            this.Controls.Add(this.txtAM);
            this.Controls.Add(this.txtAP);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnAH);
            this.Controls.Add(this.btnSalario);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AdminEmpleados";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Load += new System.EventHandler(this.AdminEmpleados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvE)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvx)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHorario)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.huella)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalario)).EndInit();
            this.panelSalario.ResumeLayout(false);
            this.panelSalario.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvE;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtAP;
        private System.Windows.Forms.TextBox txtAM;
        private System.Windows.Forms.TextBox txtDirec;
        private System.Windows.Forms.DataGridView dgvx;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbDia;
        private System.Windows.Forms.PictureBox btnHorario;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtHE;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnElH;
        private System.Windows.Forms.Button btnAgH;
        private System.Windows.Forms.DataGridView dgvH;
        private System.Windows.Forms.DateTimePicker dtHS;
        private System.Windows.Forms.Button btnAceptarH;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvH_Cdia;
        private System.Windows.Forms.DataGridViewTextBoxColumn DgvH_CHE;
        private System.Windows.Forms.DataGridViewTextBoxColumn DgvH_CHS;
        private System.Windows.Forms.Button btnAH;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblInstrucciones;
        private System.Windows.Forms.PictureBox huella;
        private System.Windows.Forms.Button btn_sal;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.PictureBox btnSalario;
        private System.Windows.Forms.Panel panelSalario;
        private System.Windows.Forms.Button btnSAceptar;
        private System.Windows.Forms.TextBox txtSTAb;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtSAb;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtST;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSA;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSF;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSD;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtSJ;
        private System.Windows.Forms.Label label14;
    }
}