﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using DPFP;
using DPFP.Capture;

namespace CentralOfertas
{
    public partial class AdminEmpleados : Form, DPFP.Capture.EventHandler
    {
        int accion = 0;
        int id, cont = 0;
        String nombre = "";

        private DPFP.Template Template;

        delegate void Function();
        private DPFP.Processing.Enrollment Enroller = new DPFP.Processing.Enrollment();
        private DPFP.Capture.Capture Capturer;
        private DPFP.Verification.Verification Verificator;

        public AdminEmpleados()
        {
            InitializeComponent();
            LlTabla();
        }

        protected virtual void Init()
        {
            try
            {
                Capturer = new DPFP.Capture.Capture();				// Create a capture operation.

                if (null != Capturer)

                    Capturer.EventHandler = this;       // Subscribe for capturing events.


                else
                    MessageBox.Show("No se puede iniciar la operación de captura");
            }
            catch
            {
                MessageBox.Show("No se puede iniciar la operación de captura", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void Start()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StartCapture();
                    lblInstrucciones.Text = "Use el lector para escanear su huella digital.";
                }
                catch
                {
                    lblInstrucciones.Text = "No se puede iniciar la captura!";
                }
            }
        }

        protected void Stop()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StopCapture();
                }
                catch
                {
                    lblInstrucciones.Text = "No se pudo terminar la captura";
                }
            }
        }

        public void procesar(DPFP.Sample Sample)
        {

            DrawPicture(ConvertSampleToBitmap(Sample));

            DPFP.FeatureSet features = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Enrollment);
            // Check quality of the sample and add to enroller if it's good
            if (features != null) try
                {
                    //    lblInstrucciones.Text = "The fingerprint feature set was created.";

                    Enroller.AddFeatures(features);		// Add feature set to template.

                }
                finally
                {
                    UpdateStatus();

                    // Check if template has been created.
                    switch (Enroller.TemplateStatus)
                    {
                        case DPFP.Processing.Enrollment.Status.Ready:	// report success and stop capturing
                            OnTemplate(Enroller.Template);
                            //      lblInstrucciones.Text = "Click Close, and then click Fingerprint Verification.";
                            //btnAceptarGrabar.Enabled = true;

                            //activaBtn();
                            Stop();
                            break;

                        case DPFP.Processing.Enrollment.Status.Failed:	// report failure and restart capturing
                            Enroller.Clear();
                            Stop();
                            UpdateStatus();
                            OnTemplate(null);
                            Start();
                            break;
                    }
                }
        }

        public DPFP.FeatureSet ExtractFeatures(DPFP.Sample Sample, DPFP.Processing.DataPurpose Purpose)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();	// Create a feature extractor
            DPFP.Capture.CaptureFeedback feedback = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet features = new DPFP.FeatureSet();
            Extractor.CreateFeatureSet(Sample, Purpose, ref feedback, ref features);			// TODO: return features as a result?
            if (feedback == DPFP.Capture.CaptureFeedback.Good)

                return features;
            else
                return null;
        }

        private void OnTemplate(DPFP.Template template)
        {
            Template = template;

            if (Template != null)
                MessageBox.Show("La muestra esta lista para guardar", "Fingerprint Enrollment"); 
            else
                MessageBox.Show("The fingerprint template is not valid. Repeat fingerprint enrollment.", "Fingerprint Enrollment");

        }

        public Bitmap ConvertSampleToBitmap(DPFP.Sample Sample)
        {
            DPFP.Capture.SampleConversion Convertor = new DPFP.Capture.SampleConversion();	// Create a sample convertor.
            Bitmap bitmap = null;												            // TODO: the size doesn't matter
            Convertor.ConvertToPicture(Sample, ref bitmap);									// TODO: return bitmap as a result
            return bitmap;
        }


        public void DrawPicture(Bitmap bitmap)
        {
            huella.Image = new Bitmap(bitmap, huella.Size);

        }


        private void UpdateStatus()
        {
            // Show number of samples needed.
            SetStatus(String.Format("Muestras requeridas: {0}", Enroller.FeaturesNeeded));

        }

        protected void SetStatus(string status)
        {
            this.Invoke(new Function(delegate ()
            {
                lblInstrucciones.Text = status;

            }));
        }



        void LlTabla()
        {
            dgvE.DataSource = ctrl.recibir("SELECT id, CONCAT(nombre,' ',ap) AS 'Nombre' FROM empleados;");
            txtNom.Text = txtAP.Text = txtAM.Text = txtDirec.Text = "";
            txtSA.Text = txtST.Text = txtSAb.Text = txtSTAb.Text = "1";
            txtSJ.Text = txtSD.Text = txtSF.Text = "0";
            dtHE.Text = "08:00:00";
            dtHS.Text = "20:00:00";
            dgvH.DataSource = null;
            dgvH.Columns.Clear();
            dgvH.Columns.Add("dgvH_CD","Día");
            dgvH.Columns.Add("dgvH_CE", "Entrada");
            dgvH.Columns.Add("dgvH_CS", "Salida");
            accion = 0;
            btnEditar.Text = "Editar";
        }

        bool comprobar()
        {
            bool r = true;
            if (txtNom.Text.Equals("") || txtAP.Text.Equals("") || txtAM.Text.Equals("") || txtDirec.Text.Equals("") || txtSA.Text.Equals("") || txtST.Text.Equals("") || txtSAb.Text.Equals("") || txtSTAb.Text.Equals("") || txtSJ.Text.Equals("") || txtSD.Text.Equals("") || txtSF.Text.Equals(""))
            {
                r = false;
                MessageBox.Show("Llene todos los campos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            try
            {
                double rx = 1;
                TextBox[] tbn = { txtSA, txtST, txtSAb, txtSTAb, txtSJ, txtSD, txtSF };
                for (int i = 0; i < tbn.Length; i++) rx = double.Parse(tbn[i].Text) / 2;
            }
            catch (Exception e)
            {
                r = false;
                MessageBox.Show("Los campos de salario deben contener sólo numeros", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            return r;
        }
        
        private void btnSalir_Click(object sender, EventArgs e)
        {
            Stop();
            this.Hide();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int i1=0,i2=0,i3=0;
            if (comprobar()) if (accion == 0)
                {
                    i1 = ctrl.enviar("INSERT INTO empleados VALUES(null,'" + txtNom.Text + "','" + txtAP.Text + "','" + txtAM.Text + "','" + txtDirec.Text + "','huella');");
                    dgvx.DataSource = ctrl.recibir("SELECT id FROM empleados WHERE nombre='" + txtNom.Text + "' AND ap='" + txtAP.Text + "' AND am='" + txtAM.Text + "' AND direccion='" + txtDirec.Text + "' AND img_h='huella';");
                    id = Int32.Parse(dgvx.Rows[0].Cells[0].Value.ToString());
                    if (dgvH.RowCount > 0)
                    {
                        for (int i = 0; i < dgvH.RowCount; i++) i2 += ctrl.enviar("INSERT INTO horarios VALUES(" + id + ",'" + dgvH.Rows[i].Cells[0].Value + "','" + dgvH.Rows[i].Cells[1].Value + "','" + dgvH.Rows[i].Cells[2].Value + "');");
                        TextBox[] tbn = { txtSA, txtST, txtSAb, txtSTAb, txtSJ, txtSD, txtSF };
                        string[] tipo = { "A", "T", "Ab", "T-Ab", "J", "D", "F" };
                        for (int i = 0; i < tbn.Length; i++) i3 += ctrl.enviar("INSERT INTO salarios VALUES(" + id + ",'" + tipo[i] + "'," + tbn[i].Text + ");");
                    }
                    else
                    {
                        i2 = 1;
                        i3 = 1;
                    }
                }
                else
                {
                    i1 = ctrl.enviar("UPDATE empleados SET nombre='" + txtNom.Text + "', ap='" + txtAP.Text + "', am='" + txtAM.Text + "', direccion='" + txtDirec.Text + "' WHERE id=" + id + ";");
                    if (dgvH.RowCount > 0)
                    {
                        i2 += ctrl.enviar("DELETE FROM horarios WHERE id_empleado=" + id + ";");
                        for (int i = 0; i < dgvH.RowCount; i++) i2 += ctrl.enviar("INSERT INTO horarios VALUES(" + id + ",'" + dgvH.Rows[i].Cells[0].Value + "','" + dgvH.Rows[i].Cells[1].Value + "','" + dgvH.Rows[i].Cells[2].Value + "');");
                        i3 += ctrl.enviar("DELETE FROM salarios WHERE id_empleado=" + id + ";");
                        TextBox[] tbn = { txtSA, txtST, txtSAb, txtSTAb, txtSJ, txtSD, txtSF };
                        string[] tipo = { "A", "T", "Ab", "T-Ab", "J", "D", "F" };
                        for (int i = 0; i < tbn.Length; i++) i3 += ctrl.enviar("INSERT INTO salarios VALUES(" + id + ",'" + tipo[i] + "'," + tbn[i].Text + ");");
                    }
                    else
                    {
                        i2 = 1;
                        i3 = 1;
                    }
                }
            if (i1 + i2 + i3 > 2)
            {
                MessageBox.Show("Operación Exitosa", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LlTabla();
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (btnEditar.Text.Equals("Editar"))
            {
                if (dgvE.SelectedRows.Count == 1)
                {
                    accion = 1;
                    btnEditar.Text = "Cancelar Edición";
                    id = Convert.ToInt32(dgvE.CurrentRow.Cells[0].Value);
                    dgvx.DataSource = ctrl.recibir("SELECT * FROM empleados WHERE id=" + id + ";");
                    txtNom.Text = dgvx.Rows[0].Cells[1].Value.ToString();
                    txtAP.Text = dgvx.Rows[0].Cells[2].Value.ToString();
                    txtAM.Text = dgvx.Rows[0].Cells[3].Value.ToString();
                    txtDirec.Text = dgvx.Rows[0].Cells[4].Value.ToString();
                    dgvH.DataSource = null;
                    dgvx.DataSource = ctrl.recibir("SELECT dia AS 'Día', he AS 'Entrada', hs AS 'Salida' FROM horarios WHERE id_empleado=" + id + ";");
                    for (int i = 0; i < dgvx.RowCount - 1; i++) dgvH.Rows.Add(dgvx.Rows[i].Cells[0].Value, dgvx.Rows[i].Cells[1].Value, dgvx.Rows[i].Cells[2].Value);
                    TextBox[] tbn = { txtSA, txtST, txtSAb, txtSTAb, txtSJ, txtSD, txtSF };
                    dgvx.DataSource = ctrl.recibir("SELECT salario FROM salarios WHERE id_empleado=" + id + ";");
                    for (int j = 0; j < dgvx.RowCount - 1; j++) tbn[j].Text = dgvx.Rows[j].Cells[0].Value.ToString();
                }
                else MessageBox.Show("Debe seleccionar una fila de la tabla", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else LlTabla();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int i1 = 0, i2 = 0, i3 = 0;
            if (dgvE.SelectedRows.Count == 1)
            {
                i3 = ctrl.enviar("DELETE FROM salarios WHERE id_empleado=" + dgvE.CurrentRow.Cells[0].Value + ";");
                i2 = ctrl.enviar("DELETE FROM horarios WHERE id_empleado=" + dgvE.CurrentRow.Cells[0].Value + ";");
                i1 = ctrl.enviar("DELETE FROM empleados WHERE id=" + dgvE.CurrentRow.Cells[0].Value + ";");
                //eliminar imagen .fpt
            }
            else MessageBox.Show("Debe seleccionar una fila de la tabla", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            if (i1 + i2 + i3> 2)
            {
                MessageBox.Show("Eliminación Exitosa", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LlTabla();
            }
        }

        private void btnAgH_Click(object sender, EventArgs e)
        {
            bool h = true;
            for (int i = 0; i < dgvH.RowCount - 1; i++) if (dgvH.Rows[i].Cells[0].Value.ToString().Equals(cbDia.SelectedItem))
                {
                    h = false;
                    MessageBox.Show("El día "+cbDia.SelectedItem+" ya está en la lista", "Día", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            if (cbDia.Text.Equals(""))
            {
                h = false;
                MessageBox.Show("Debe elegir un día válido de la lista", "Día", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            if (DateTime.Compare(dtHE.Value, dtHS.Value) != -1) 
            {
                h = false;
                MessageBox.Show("La hora de salida debe ser nayor que la hora de entrada", "Hora", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            if(h)dgvH.Rows.Add(cbDia.SelectedItem,dtHE.Text,dtHS.Text);
        }

        private void btnHorario_Click(object sender, EventArgs e)
        {
            panel.Visible = true;
        }

        private void btnAceptarH_Click(object sender, EventArgs e)
        {
            panel.Visible = false;
        }

        private void btnElH_Click(object sender, EventArgs e)
        {
            if (dgvH.SelectedRows.Count == 1)dgvH.Rows.Remove(dgvH.CurrentRow);
            else MessageBox.Show("Debe seleccionar una fila de la tabla", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }



        public void OnComplete(object Capture, string ReaderSerialNumber, Sample Sample)
        {
            procesar(Sample);
            //throw new NotImplementedException();
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
            //throw new NotImplementedException();
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {
            //throw new NotImplementedException();
        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
            //throw new NotImplementedException();
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {
            //throw new NotImplementedException();
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, CaptureFeedback CaptureFeedback)
        {
            if (CaptureFeedback == DPFP.Capture.CaptureFeedback.Good)
                lblInstrucciones.Text = "La calidad de la muestra de impresion de dedo es buena.";
            else
                lblInstrucciones.Text = "La calidad de la muestra de impresion de dedo es pobre.";

            //throw new NotImplementedException();
        }

        private void btnAH_Click(object sender, EventArgs e)
        {
            if (dgvE.SelectedRows.Count == 1)
            {
                id=Int32.Parse(dgvE.CurrentRow.Cells[0].Value.ToString());
                panel1.Visible = true;
            }
            else MessageBox.Show("Debe seleccionar un empleado de la lista");            
        }

        private void AdminEmpleados_Load(object sender, EventArgs e)
        {
            try
            {
                Init();
                Start();

            }
            catch
            {
                lblInstrucciones.Text = "No se puede iniciar la captura!";
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
            Stop();
            this.Hide();
            new AdminEmpleados().Show();
        }

        private void btn_sal_Click(object sender, EventArgs e)
        {
            using (FileStream fs = File.Open("huellas\\h_" + id + ".fpt", FileMode.Create, FileAccess.Write))
            {
                Template.Serialize(fs);
            }
            if (ctrl.enviar("UPDATE empleados SET img_h='h_" + id + ".fpt' WHERE id=" + id + ";") > 0) MessageBox.Show("Guardado exitoso", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); ;
            panel1.Visible = false;
            huella.Image = null;
            Stop();
            this.Hide();
            new AdminEmpleados().Show();
        }

        private void btnSalario_Click(object sender, EventArgs e)
        {
            panelSalario.Visible = true;
        }

        private void btnSAceptar_Click(object sender, EventArgs e)
        {
            panelSalario.Visible = false;
        }
    }
}