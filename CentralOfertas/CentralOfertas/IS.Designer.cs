﻿namespace CentralOfertas
{
    partial class IS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.crvIS = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.btnVer = new System.Windows.Forms.Button();
            this.lblM = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // crvIS
            // 
            this.crvIS.ActiveViewIndex = -1;
            this.crvIS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvIS.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvIS.DisplayStatusBar = false;
            this.crvIS.Location = new System.Drawing.Point(0, 42);
            this.crvIS.Name = "crvIS";
            this.crvIS.Size = new System.Drawing.Size(949, 501);
            this.crvIS.TabIndex = 0;
            this.crvIS.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // btnVer
            // 
            this.btnVer.BackColor = System.Drawing.Color.Lime;
            this.btnVer.Location = new System.Drawing.Point(386, 12);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(163, 23);
            this.btnVer.TabIndex = 1;
            this.btnVer.Text = "Recargar Informe Semanal";
            this.btnVer.UseVisualStyleBackColor = false;
            this.btnVer.Click += new System.EventHandler(this.btnVer_Click);
            // 
            // lblM
            // 
            this.lblM.AutoSize = true;
            this.lblM.Location = new System.Drawing.Point(9, 23);
            this.lblM.Name = "lblM";
            this.lblM.Size = new System.Drawing.Size(89, 13);
            this.lblM.TabIndex = 2;
            this.lblM.Text = "Informe Semanal:";
            // 
            // IS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 543);
            this.Controls.Add(this.lblM);
            this.Controls.Add(this.btnVer);
            this.Controls.Add(this.crvIS);
            this.Name = "IS";
            this.Text = "Infrome Semanal de Asistencias";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvIS;
        private System.Windows.Forms.Button btnVer;
        private System.Windows.Forms.Label lblM;
    }
}