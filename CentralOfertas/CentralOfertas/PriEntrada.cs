﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DPFP;
using DPFP.Capture;

namespace CentralOfertas
{
    public partial class PriEntrada : Form, DPFP.Capture.EventHandler
    {
        //huella
        private DPFP.Template Template;
        delegate void Function();
        private DPFP.Processing.Enrollment Enroller = new DPFP.Processing.Enrollment();
        private DPFP.Capture.Capture Capturer;
        private DPFP.Verification.Verification Verificator = new DPFP.Verification.Verification();
        //huella
        Timer t = new Timer();
        Timer tmr = new Timer();
        bool registro;
        public PriEntrada()
        {
            InitializeComponent();
            t.Tick += new System.EventHandler(t_Tick);
            t.Interval = 2000;
            tmr.Tick += new System.EventHandler(tmr_Tick);
            tmr.Interval = 1800000;//1800000
            iniciar();
        }

        //huella
        protected virtual void Init()
        {
            try
            {
                Capturer = new DPFP.Capture.Capture();				// Create a capture operation.
                if (null != Capturer) Capturer.EventHandler = this;		// Subscribe for capturing events.
                else MessageBox.Show("Can't initiate capture operation!");
            }
            catch
            {
                MessageBox.Show("Can't initiate capture operation!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        protected void Start()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StartCapture();
                    lblInstrucciones.Text = "Use el lector para escanear su huella digital.";
                }
                catch
                {
                    lblInstrucciones.Text = "No se puede iniciar la captura";
                }
            }
        }
        protected void Stop()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StopCapture();
                }
                catch
                {
                    lblInstrucciones.Text = "Can't terminate capture!";
                }
            }
        }
        public void OnComplete(object Capture, string ReaderSerialNumber, Sample Sample)
        {
            if (registro)leeRutas(Sample);
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber) { }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber) { }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber) { }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber) { }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, CaptureFeedback CaptureFeedback)
        {
            if (CaptureFeedback == DPFP.Capture.CaptureFeedback.Good) lblInstrucciones.Text = "The quality of the fingerprint sample is good.";
            else lblInstrucciones.Text = "The quality of the fingerprint sample is poor.";
        }
        //huella

        //Acceso
        public void leeRutas(DPFP.Sample Sample)
        {
            String id = "", nombre="";
            bool encontrado = false;
            FileStream fsTG;
                
                dgv.DataSource = ctrl.recibir("SELECT id, img_h FROM empleados;");
            int i = 0;
                for (i = 0; i < dgv.RowCount - 1; i++)
                {
                    nombre = dgv.Rows[i].Cells[1].Value.ToString();
                    fsTG = File.OpenRead("huellas\\" + nombre);
                    
                    DPFP.Template templateGuardado = new DPFP.Template(fsTG);

                    if (procesar(Sample, templateGuardado) == "Bienvenido")
                    {
                    id = dgv.Rows[i].Cells[0].Value.ToString();
                    encontrado = true;
                    verificar(id);
                        break;
                    }
                } 
                if(!encontrado) MessageBox.Show("No se encontró a este empleado", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        public DPFP.FeatureSet ExtractFeatures(DPFP.Sample Sample, DPFP.Processing.DataPurpose Purpose)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();	// Create a feature extractor
            DPFP.Capture.CaptureFeedback feedback = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet features = new DPFP.FeatureSet();
            Extractor.CreateFeatureSet(Sample, Purpose, ref feedback, ref features);			// TODO: return features as a result?
            if (feedback == DPFP.Capture.CaptureFeedback.Good) return features;
            else return null;
        }

        public string procesar(DPFP.Sample Sample, DPFP.Template templateGuardado)
        {
            DrawPicture(ConvertSampleToBitmap(Sample));
            DPFP.FeatureSet features = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Verification);

            if (features != null)
            {
                DPFP.Verification.Verification.Result result = new DPFP.Verification.Verification.Result();
                Verificator.Verify(features, templateGuardado, ref result);
                if (result.Verified) return "Bienvenido";
                else return "Fail";
            }
            else return "Fail";
        }

        public Bitmap ConvertSampleToBitmap(DPFP.Sample Sample)
        {
            DPFP.Capture.SampleConversion Convertor = new DPFP.Capture.SampleConversion();	// Create a sample convertor.
            Bitmap bitmap = null;												            // TODO: the size doesn't matter
            Convertor.ConvertToPicture(Sample, ref bitmap);									// TODO: return bitmap as a result
            return bitmap;
        }

        public void DrawPicture(Bitmap bitmap)
        {
            Picture.Image = new Bitmap(bitmap, Picture.Size);
        }
        //Acceso

        private void t_Tick(object sender, EventArgs e)
        {
            txtFecha.Clear();
            txtHora.Clear();
            txtNom.Clear();
            txtAp.Clear();
            Picture.Image = null;
            t.Stop();
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now.DayOfWeek.ToString().Equals("Saturday") && DateTime.Now.Hour.ToString().Equals("18")) new IS().ShowDialog();
        }

        bool comprobar()
        {
            dgv.DataSource = ctrl.recibir("SELECT id FROM empleados;");
            if (dgv.RowCount > 1) return true;
            else
            {
                MessageBox.Show("Se debe registrar a los empleados","Advertencia",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return false;
            }
        }
        void unico()
        {
            dgv.DataSource = ctrl.recibir("SELECT id FROM empleados;");
            string[] ids = new string[dgv.RowCount - 1];
            for (int i = 0; i < dgv.RowCount - 1; i++) ids[i] = dgv.Rows[i].Cells[0].Value.ToString();
            for (int i = 0; i < ids.Length; i++)
            {
                bool n = true;
                dgv.DataSource = ctrl.recibir("SELECT dia FROM horarios WHERE id_empleado=" + ids[i] + ";");
                string[] dias = new string[dgv.RowCount - 1];
                for (int j = 0; j < dgv.RowCount - 1; j++) dias[j] = dgv.Rows[j].Cells[0].Value.ToString();
                for (int k = 0; k < dias.Length; k++)
                {
                    if (ctrl.dia.Equals(dias[k]))
                    {
                        n = false;
                        break;
                    }
                }
                dgv.DataSource = ctrl.recibir("SELECT id FROM asistencias WHERE id_empleado=" + ids[i] + " AND fecha='" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "';");
                if (dgv.RowCount < 2) if (n) ctrl.enviar("INSERT INTO asistencias VALUES(null,'D','" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "'," + ids[i] + ");");
                    else ctrl.enviar("INSERT INTO asistencias VALUES(null,'F','" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "'," + ids[i] + ");");   
            }
        }

        void iniciar()
        {
            registro = comprobar();
            string dia = DateTime.Now.DayOfWeek.ToString();
            switch (dia)
            {
                case "Sunday":
                    dia = "Domingo";
                    break;
                case "Monday":
                    dia = "Lunes";
                    break;
                case "Tuesday":
                    dia = "Martes";
                    break;
                case "Wednesday":
                    dia = "Miercoles";
                    break;
                case "Thursday":
                    dia = "Jueves";
                    break;
                case "Friday":
                    dia = "Viernes";
                    break;
                case "Saturday":
                    dia = "Sabado";
                    break;
            }
            ctrl.dia = dia;
            if (registro) unico();
            tmr.Start();
        }

        void opr(string id)
        {
            dgv.DataSource = ctrl.recibir("SELECT fecha,hora FROM registros WHERE id_empleado="+id+" ORDER BY id DESC LIMIT 2;");
            if (dgv.RowCount > 2) if (DateTime.Compare(DateTime.Parse(dgv.Rows[0].Cells[0].Value.ToString()), DateTime.Parse(dgv.Rows[1].Cells[0].Value.ToString())) != 0)
                {
                    ctrl.enviar("INSERT INTO registros VALUES(null,'Salida','" + dgv.Rows[1].Cells[0].Value + "','" + dgv.Rows[1].Cells[1].Value + "'," + id + ");");
                    ctrl.enviar("INSERT INTO asistencias VALUES(null,'Ab','" + dgv.Rows[1].Cells[0].Value + "'," + id + ");");
                }
            if (ctrl.enviar("INSERT INTO registros VALUES(null,'Entrada','" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "','" + DateTime.Now.ToLongTimeString() + "'," + id + ");") > 0) intermit(id);
        }

        void verificar(string id)
        {
            if (registro) 
            {
                     dgv.DataSource = ctrl.recibir("SELECT he FROM horarios WHERE id_empleado=" + id + " AND dia='" + ctrl.dia + "';");
                     if (dgv.RowCount > 1)
                     {
                         dgv.DataSource = ctrl.recibir("SELECT id FROM registros WHERE id_empleado=" + id + " AND fecha='" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "' AND tipo='Entrada';");
                         if (dgv.RowCount < 2)
                         {
                            dgv.DataSource = ctrl.recibir("SELECT fecha, hora FROM registros ORDER BY id LIMIT 1;");
                            if (dgv.RowCount < 2)
                            {
                            if (ctrl.enviar("INSERT INTO registros VALUES(null,'Entrada','" + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "','" + DateTime.Now.ToLongTimeString() + "'," + id + ");") > 0)  intermit(id);
                            }
                            else
                            {
                                 string fb = DateTime.Parse(dgv.Rows[0].Cells[0].Value.ToString()).ToShortDateString() + " " + dgv.Rows[0].Cells[1].Value;
                                 if (DateTime.Compare(DateTime.Parse(fb), DateTime.Now) == -1) opr(id);
                                 else MessageBox.Show("Error al tratar de registrar acceso", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else MessageBox.Show("Usted ya registró su Entrada", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else MessageBox.Show("Usted tiene aisgnado Descanso en este día", "Información", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
        }

        void intermit(string id)
        {
            dgv.DataSource = ctrl.recibir("SELECT nombre, CONCAT(ap,' ',am) FROM empleados WHERE id="+id+";");
            txtFecha.Text = DateTime.Now.ToShortDateString();
            txtHora.Text = DateTime.Now.ToLongTimeString();
            txtNom.Text = dgv.Rows[0].Cells[0].Value.ToString();
            txtAp.Text = dgv.Rows[0].Cells[1].Value.ToString();
            MessageBox.Show("B I E N V E N I D O", "CENTRAL DE OFERTAS", MessageBoxButtons.OK, MessageBoxIcon.Information);
            t.Start();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Stop();
            Application.Exit();
        }

        private void btnSalida_Click(object sender, EventArgs e)
        {
            Stop();
            new PriSalida().Show();
            this.Hide();
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            Stop();
            ctrl.origen = "Entrada";
            new Login().Show();
            this.Hide();
        }


        private void PriEntrada_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            try
            {
                Init();
                Start();
            }
            catch
            {
                lblInstrucciones.Text = "No se puede iniciar la captura!";
            }
        }
    }
}