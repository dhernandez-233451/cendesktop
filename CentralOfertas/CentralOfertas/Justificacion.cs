﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CentralOfertas
{
    public partial class Justificacion : Form
    {
        public Justificacion()
        {
            InitializeComponent();
            LlTG();
        }

        void LlTG()
        {
            dgvF.DataSource = ctrl.recibir("SELECT asistencias.id, CONCAT(nombre,' ',ap) AS 'Nombre', asistencia AS 'Asistencia', fecha AS 'Fecha' FROM asistencias,empleados WHERE asistencia!='A' AND asistencia!='J' AND empleados.id=asistencias.id_empleado ORDER BY fecha;");
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (chkF.Checked && !txtNom.Text.Equals("")) dgvF.DataSource = ctrl.recibir("SELECT asistencias.id, CONCAT(nombre,' ',ap) AS 'Nombre', asistencia AS 'Asistencia', fecha AS 'Fecha' FROM asistencias,empleados WHERE asistencia!='A' AND asistencia!='J' AND empleados.id=asistencias.id_empleado AND fecha='" + dtFecha.Value.Year + "-" + dtFecha.Value.Month + "-" + dtFecha.Value.Day + "' AND CONCAT(nombre,' ',ap,' ',am) LIKE '%"+txtNom.Text+"%';");
            if (chkF.Checked && txtNom.Text.Equals("")) dgvF.DataSource = ctrl.recibir("SELECT asistencias.id, CONCAT(nombre,' ',ap) AS 'Nombre', asistencia AS 'Asistencia', fecha AS 'Fecha' FROM asistencias,empleados WHERE asistencia!='A' AND asistencia!='J' AND empleados.id=asistencias.id_empleado AND fecha='" + dtFecha.Value.Year + "-" + dtFecha.Value.Month + "-" + dtFecha.Value.Day + "';");
            if (!chkF.Checked && !txtNom.Text.Equals("")) dgvF.DataSource = ctrl.recibir("SELECT asistencias.id, CONCAT(nombre,' ',ap) AS 'Nombre', asistencia AS 'Asistencia', fecha AS 'Fecha' FROM asistencias,empleados WHERE asistencia!='A' AND asistencia!='J' AND empleados.id=asistencias.id_empleado AND CONCAT(nombre,' ',ap,' ',am) LIKE '%" + txtNom.Text + "%';");
            if (!chkF.Checked && txtNom.Text.Equals("")) LlTG();
        }

        private void btnJus_Click(object sender, EventArgs e)
        {
            int c1 = 0, c2 = 0;
            if (dgvF.SelectedRows.Count == 1)
            {
                if (cbMotivo.SelectedItem.ToString().Equals("Otro"))
                {
                    if (txtOtro.Text.Equals(""))MessageBox.Show("Escriba el motivo en la caja de texto", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    else {
                        c1=ctrl.enviar("UPDATE asistencias SET asistencia='J' WHERE id=" + dgvF.CurrentRow.Cells[0].Value + ";");
                        c2 = ctrl.enviar("INSERT INTO justifics VALUES(null," + dgvF.CurrentRow.Cells[0].Value + ",'" + txtOtro.Text + "');");
                    }
                }
                else if (cbMotivo.SelectedIndex >= 0)
                {
                    c1 = ctrl.enviar("UPDATE asistencias SET asistencia='J' WHERE id=" + dgvF.CurrentRow.Cells[0].Value + ";");
                    c2 = ctrl.enviar("INSERT INTO justifics VALUES(null," + dgvF.CurrentRow.Cells[0].Value + ",'" + cbMotivo.SelectedItem.ToString() + "');");
                }
                if (c1+c1 > 1) MessageBox.Show("Justificación Exitosa", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                else MessageBox.Show("Error al tratar de Justificar", "Información", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else MessageBox.Show("Debe seleccionar una fila de la tabla", "Información", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            LlTG();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void cbMotivo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbMotivo.SelectedItem.ToString().Equals("Otro"))txtOtro.Visible = true;
            else txtOtro.Visible = false;
        }
    }
}
