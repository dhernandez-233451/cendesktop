﻿namespace CentralOfertas
{
    partial class Estadistica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dtFI = new System.Windows.Forms.DateTimePicker();
            this.dtFF = new System.Windows.Forms.DateTimePicker();
            this.btnSalir = new System.Windows.Forms.Button();
            this.crvE = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.cbTipo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtNombre
            // 
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(632, 65);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(128, 19);
            this.txtNombre.TabIndex = 2;
            // 
            // btnBuscar
            // 
            this.btnBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.Location = new System.Drawing.Point(810, 49);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 5;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // dtFI
            // 
            this.dtFI.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFI.Location = new System.Drawing.Point(233, 62);
            this.dtFI.Name = "dtFI";
            this.dtFI.Size = new System.Drawing.Size(106, 20);
            this.dtFI.TabIndex = 6;
            // 
            // dtFF
            // 
            this.dtFF.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFF.Location = new System.Drawing.Point(439, 62);
            this.dtFF.Name = "dtFF";
            this.dtFF.Size = new System.Drawing.Size(102, 20);
            this.dtFF.TabIndex = 7;
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(810, 75);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 8;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // crvE
            // 
            this.crvE.ActiveViewIndex = -1;
            this.crvE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crvE.Cursor = System.Windows.Forms.Cursors.Default;
            this.crvE.DisplayBackgroundEdge = false;
            this.crvE.DisplayStatusBar = false;
            this.crvE.ForeColor = System.Drawing.SystemColors.ControlText;
            this.crvE.Location = new System.Drawing.Point(13, 128);
            this.crvE.Name = "crvE";
            this.crvE.Size = new System.Drawing.Size(887, 408);
            this.crvE.TabIndex = 9;
            this.crvE.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
            // 
            // cbTipo
            // 
            this.cbTipo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.cbTipo.FormattingEnabled = true;
            this.cbTipo.Items.AddRange(new object[] {
            "General",
            "Por fecha y Empleado",
            "De horas de entradas y salidas",
            "Justificación"});
            this.cbTipo.Location = new System.Drawing.Point(13, 61);
            this.cbTipo.Name = "cbTipo";
            this.cbTipo.Size = new System.Drawing.Size(127, 21);
            this.cbTipo.TabIndex = 12;
            // 
            // Estadistica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CentralOfertas.Properties.Resources.estadistica;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(912, 548);
            this.Controls.Add(this.cbTipo);
            this.Controls.Add(this.crvE);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.dtFF);
            this.Controls.Add(this.dtFI);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtNombre);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Estadistica";
            this.Text = "Estadistica";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DateTimePicker dtFI;
        private System.Windows.Forms.DateTimePicker dtFF;
        private System.Windows.Forms.Button btnSalir;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crvE;
        private System.Windows.Forms.ComboBox cbTipo;

    }
}