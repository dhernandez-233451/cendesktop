﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CentralOfertas
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            //txtU.Text = txtC.Text = "Admon";
        }

        private void picCancelar_Click(object sender, EventArgs e)
        {
            if (ctrl.origen.Equals("Salida"))
            {
                new PriSalida().Show();
                this.Hide();
            }
            else
            {
                new PriEntrada().Show();
                this.Hide();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            int i = 0;
            bool encontrado = false;
            string id="";
            dgvx.DataSource = ctrl.recibir("SELECT id, usuario, CAST(AES_DECRYPT(contra,'cod0417') AS CHAR(15))contra FROM admon;");
            for (i = 0; i < dgvx.RowCount - 1;i++ )if (dgvx.Rows[i].Cells[1].Value.Equals(txtU.Text) && dgvx.Rows[i].Cells[2].Value.Equals(txtC.Text))
                {
                    encontrado = true;
                    id = dgvx.Rows[i].Cells[0].Value.ToString();
                }
            if (encontrado)
            {
                switch (id)
                {
                    case "1":
                        new MenuConfi().Show();
                        break;
                    case "2":
                        new Menusuario().Show();
                        break;
                }
                this.Hide();
            }
            else MessageBox.Show("Usuario o Contraseña invalido(a)");
        }
    }
}
