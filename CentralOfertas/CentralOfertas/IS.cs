﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CentralOfertas
{
    public partial class IS : Form
    {
        public IS()
        {
            InitializeComponent();
            lblM.Text += DateTime.Now.AddDays(-6).ToShortDateString() + " - " + DateTime.Now.ToShortDateString();
            cargar();
        }
        
        private void btnVer_Click(object sender, EventArgs e)
        {
            cargar();
        }
        void cargar()
        {
            rf rf1 = new rf();
            rf1.SetParameterValue("prf1", DateTime.Now.AddDays(-6).ToShortDateString());
            rf1.SetParameterValue("prf2", DateTime.Now.ToShortDateString());
            crvIS.ReportSource = rf1;
            crvIS.Refresh();
        }
    }
}
