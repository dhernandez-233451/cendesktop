﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;

namespace CentralOfertas
{
    class ctrl
    {
        static MySqlConnection conectar;
        public static string origen="";
        public static string dia = "";
        public static MySqlConnection Conexion()
        {
            conectar = new MySqlConnection("server=127.0.0.1; database=co; Uid=root; pwd=;");
            conectar.Open();
            return conectar;
        }
        public static int enviar(string cadena)
        {
            int retorno = 0;
            try
            {
                MySqlCommand comando = new MySqlCommand(cadena, Conexion());
                retorno = comando.ExecuteNonQuery();
                conectar.Close();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e+"","Error de acceso a BD");
            }
            return retorno;
        }
        public static DataTable recibir(string cadena){
            DataTable tabla = new DataTable();
            try
            {
                MySqlDataAdapter da = new MySqlDataAdapter(cadena, Conexion());
                da.Fill(tabla);
                conectar.Close();
            }
            catch (MySqlException e)
            {
                MessageBox.Show(e + "", "Error de acceso a BD");
            }
            return tabla;
        }
    }
}
