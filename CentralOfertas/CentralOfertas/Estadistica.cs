﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CentralOfertas
{
    public partial class Estadistica : Form
    {
        public Estadistica()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            switch (cbTipo.SelectedIndex)
            {
                case 0:
                    rf rf1 = new rf();
                    rf1.SetParameterValue("prf1", dtFI.Value);
                    rf1.SetParameterValue("prf2", dtFF.Value);
                    crvE.ReportSource = rf1;
                    break;
                case 1:
                    rfn rfn1 = new rfn();
                    rfn1.SetParameterValue("prf1", dtFI.Value);
                    rfn1.SetParameterValue("prf2", dtFF.Value);
                    rfn1.SetParameterValue("prNom", txtNombre.Text);
                    crvE.ReportSource = rfn1;
                    break;
                case 2:
                    rfnh rfnh1 = new rfnh();
                    rfnh1.SetParameterValue("prf1", dtFI.Value);
                    rfnh1.SetParameterValue("prf2", dtFF.Value);
                    rfnh1.SetParameterValue("prNom", txtNombre.Text);
                    crvE.ReportSource = rfnh1;
                    break;
                case 3:
                    rj rj1 = new rj();
                    rj1.SetParameterValue("prf1", dtFI.Value);
                    rj1.SetParameterValue("prf2", dtFF.Value);
                    rj1.SetParameterValue("prNom", txtNombre.Text);
                    crvE.ReportSource = rj1;
                    break;
            }
            crvE.Refresh();
        }
    }
}
