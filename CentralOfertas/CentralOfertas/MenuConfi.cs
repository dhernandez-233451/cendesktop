﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CentralOfertas
{
    public partial class MenuConfi : Form
    {
        public MenuConfi()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (ctrl.origen.Equals("Salida"))
            {
                new PriSalida().Show();
                this.Hide();
            }
            else
            {
                new PriEntrada().Show();
                this.Hide();
            }
        }

        private void btnAdmon_Click(object sender, EventArgs e)
        {
            new AdminEmpleados().Show();
        }

        private void btnCuenta_Click(object sender, EventArgs e)
        {
            new CuentaAdmin().Show();
        }

        private void btnEst_Click(object sender, EventArgs e)
        {
            new Estadistica().Show();
        }

        private void btnJus_Click(object sender, EventArgs e)
        {
            new Justificacion().Show();
        }
    }
}
