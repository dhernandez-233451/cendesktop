﻿namespace CentralOfertas
{
    partial class Justificacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNom = new System.Windows.Forms.TextBox();
            this.btnBuscar = new System.Windows.Forms.PictureBox();
            this.btnSalir = new System.Windows.Forms.PictureBox();
            this.btnJus = new System.Windows.Forms.PictureBox();
            this.dgvF = new System.Windows.Forms.DataGridView();
            this.dtFecha = new System.Windows.Forms.DateTimePicker();
            this.chkF = new System.Windows.Forms.CheckBox();
            this.cbMotivo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtOtro = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnJus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvF)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNom
            // 
            this.txtNom.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNom.Location = new System.Drawing.Point(185, 55);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(227, 19);
            this.txtNom.TabIndex = 0;
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackColor = System.Drawing.Color.Transparent;
            this.btnBuscar.Location = new System.Drawing.Point(461, 52);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(90, 27);
            this.btnBuscar.TabIndex = 1;
            this.btnBuscar.TabStop = false;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.Transparent;
            this.btnSalir.Location = new System.Drawing.Point(536, 366);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(90, 27);
            this.btnSalir.TabIndex = 2;
            this.btnSalir.TabStop = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnJus
            // 
            this.btnJus.BackColor = System.Drawing.Color.Transparent;
            this.btnJus.Location = new System.Drawing.Point(62, 366);
            this.btnJus.Name = "btnJus";
            this.btnJus.Size = new System.Drawing.Size(147, 27);
            this.btnJus.TabIndex = 3;
            this.btnJus.TabStop = false;
            this.btnJus.Click += new System.EventHandler(this.btnJus_Click);
            // 
            // dgvF
            // 
            this.dgvF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvF.Location = new System.Drawing.Point(45, 134);
            this.dgvF.Name = "dgvF";
            this.dgvF.Size = new System.Drawing.Size(581, 210);
            this.dgvF.TabIndex = 4;
            // 
            // dtFecha
            // 
            this.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFecha.Location = new System.Drawing.Point(66, 100);
            this.dtFecha.Name = "dtFecha";
            this.dtFecha.Size = new System.Drawing.Size(81, 20);
            this.dtFecha.TabIndex = 5;
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.BackColor = System.Drawing.SystemColors.ControlDark;
            this.chkF.Location = new System.Drawing.Point(153, 103);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(56, 17);
            this.chkF.TabIndex = 6;
            this.chkF.Text = "Fecha";
            this.chkF.UseVisualStyleBackColor = false;
            // 
            // cbMotivo
            // 
            this.cbMotivo.FormattingEnabled = true;
            this.cbMotivo.Items.AddRange(new object[] {
            "Cita médica",
            "Médico",
            "Personal",
            "Traslado",
            "Climático",
            "Vacaciones",
            "Descanso",
            "Otro"});
            this.cbMotivo.Location = new System.Drawing.Point(353, 99);
            this.cbMotivo.Name = "cbMotivo";
            this.cbMotivo.Size = new System.Drawing.Size(160, 21);
            this.cbMotivo.TabIndex = 7;
            this.cbMotivo.Text = "Eliga";
            this.cbMotivo.SelectedIndexChanged += new System.EventHandler(this.cbMotivo_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.DarkGray;
            this.label1.Location = new System.Drawing.Point(293, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Problema:";
            // 
            // txtOtro
            // 
            this.txtOtro.Location = new System.Drawing.Point(524, 99);
            this.txtOtro.Name = "txtOtro";
            this.txtOtro.Size = new System.Drawing.Size(100, 20);
            this.txtOtro.TabIndex = 9;
            this.txtOtro.Visible = false;
            // 
            // Justificacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CentralOfertas.Properties.Resources.justificacion;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(687, 405);
            this.Controls.Add(this.txtOtro);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbMotivo);
            this.Controls.Add(this.chkF);
            this.Controls.Add(this.dtFecha);
            this.Controls.Add(this.dgvF);
            this.Controls.Add(this.btnJus);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtNom);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Justificacion";
            this.Text = "Justificacion";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            ((System.ComponentModel.ISupportInitialize)(this.btnBuscar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnJus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvF)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.PictureBox btnBuscar;
        private System.Windows.Forms.PictureBox btnSalir;
        private System.Windows.Forms.PictureBox btnJus;
        private System.Windows.Forms.DataGridView dgvF;
        private System.Windows.Forms.DateTimePicker dtFecha;
        private System.Windows.Forms.CheckBox chkF;
        private System.Windows.Forms.ComboBox cbMotivo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtOtro;
    }
}